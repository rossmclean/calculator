import './App.css';
import Title from './Components/Title';
import Buttons from './Components/Buttons';
import Operators from './Components/Operators';
import OutputScreen from './Components/OutputScreen';

function App() {
  return (
    <div className="App">
      <Title/>
      <OutputScreen/>
      <Buttons/>
      <Operators/>
      
      
    </div>
  );
}

export default App;
