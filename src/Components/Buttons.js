import React from 'react'
import { useState } from 'react'

export default function Buttons() {

    const buttonGroup = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, '.']
    const operatorGroup = ["X", "-", "+", "/"]
    const interfaceGroup = ["C", "="]


    const [input, setInput] = useState({
        question: "",
        answer: 0,
        result: function () {
            this.answer = eval(this.question)
            
        }
    })

    const [output, setOutput] = useState(null)

    console.log(output)
    console.log(input)



    let handleInput = (value) => {
        if (input.question !== "") {
            setInput({...input})

        }
      setInput({...input, question: value})

    }

    let handleOperator = (oper) => {
        input.question.concat(oper)

       
    }

    let handleInterface = (button) => {
        if (button === "=") {
            input.result()
        } else if (button === "C") {


        }

    }



    return (
        <div className='btn-keypad'>

            <h1>answer = {input.answer}</h1>


           <h1>question = {input.question}</h1>


            <div>
                {buttonGroup.map((num, idx) => <button onClick={() => handleInput(num)} className='btn' value={num} key={idx}>{num}</button>)}

            </div>


            <div>{operatorGroup.map((oper, idx) => <button onClick={() => handleOperator(oper)} className='btn' value={oper} key={idx}>{oper}</button>)}</div>


            {interfaceGroup.map((button, idx) => <button onClick={() => handleInterface(button)} className='btn' value={button} key={idx}>{button}</button>)}



        </div>
    )
}
